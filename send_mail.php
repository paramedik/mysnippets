<?php

require_once ( dirname( __FILE__ )  . '/wp-load.php' );

$json = file_get_contents('php://input');
$message = json_decode($json);

if (isset($message->to)) {
 	
	// subject
	$subject = 'Betreff nicht angegeben';
	if(isset($message->subject)){
		$subject = $message->subject;
	}
	
	// body
	$body = 'Nachricht nicht vorhanden!';
	if(isset($message->body)){
		$body = $message->body;
	}
	
	// attachments	// TODO base64 special handling
	$attachments;
	if(isset($message->attachments)){
		$attachments = base64_to_file($message->attachments, 'tmp_img_' . rand( 1, 1000000000 ) . '.png');
	}else{
		$attachments = array();
	}
	
	// from
	$from = 'webmaster@mydomainname.com';
	if(isset($message->from)){
		$from = $message->from;
	}
	
	// first and lastname
	$firstlastname = 'Unbekannt';
	if(isset($message->firstlastname)){
		$firstlastname = $message->firstlastname;
	}
	
	// headers
	$headers = array( 'Reply-To: "' . $firstlastname . '" <' . $from . '>' );
	
	// hooks
	//add_filter( 'wp_mail_from', 'custom_wp_mail_from' );
	//function custom_wp_mail_from( $original_email_address ) {
		//Make sure the email is from the same domain 
		//as your website to avoid being marked as spam.
		//return 'nicht@anworten.com';
	//}
	
	add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
	function custom_wp_mail_from_name( $original_email_from ) {
		return 'App Formular - Stadt Mainburg';
	}
	
    //add_filter('wp_mail_content_type', 'set_html_content_type'));
	//function set_html_content_type() {
		//return 'text/html';
	//}
	
    $result = wp_mail($message->to, $subject, $body, $headers, $attachments);
	
	//remove_filter('wp_mail_from','custom_wp_mail_from');
	remove_filter('wp_mail_from_name','custom_wp_mail_from_name');
	//remove_filter('wp_mail_content_type', 'set_html_content_type');
	
	if(!is_array($attachments)){
		clear_attachments($attachments);	
	}
	
	if($result){
		echo 'OK';
	}else{
		echo 'NOK';
		//echo 'MESSAGE: <pre>' . var_export($message, true) . '</pre>';
	}
}else{
	echo 'Required fields not set! Email will not be send.';
}

function base64_to_file($base64_string, $output_file) {
    $ifp = fopen( $output_file, 'wb' ); 

    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $base64_string );

    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[ 1 ] ) );
    fclose( $ifp ); 
    return $output_file; 
}

function clear_attachments($attachments){
	unlink($attachments);
}