<?php
/*-----------------------------------------------------------------------------------*/
/* Info Table
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'sd_info_table_ext' ) ) {
	function sd_info_table_ext( $atts, $content = null ) {
		$sd = shortcode_atts( array(
			'header_height'   => '',
			'title'           => '',
                        'gmap_active'     => '',
                        'gmap_api_key'    => '',
                        'gmap_place_id'   => '',
			'text'            => '',
			'read_more'       => '',
			'read_more_text'  => esc_html__( 'Read More', 'digitalagency' ),
			'icon_type'       => '',
			'icon_font'       => '',
			'icon_img'        => '',
			'icon_margin'     => '',
			'link'            => '',
			'bg_color'        => '',
			'title_color'     => '',
			'text_color'      => '',
			'header_color'    => '',
			'icon_bg_color'   => '',
			'icon_color'      => '',
			'read_more_color' => '',
		), $atts );
		
		$header_height   = $sd['header_height'];
		$title           = $sd['title'];
                $gmap_active     = $sd['gmap_active'];
                $gmap_api_key    = $sd['gmap_api_key'];
                $gmap_place_id   = $sd['gmap_place_id'];
		$content         = wpb_js_remove_wpautop( $content, true );
		$text            = $sd['text'];
		$read_more       = $sd['read_more'];
		$read_more_text  = $sd['read_more_text'];
		$icon_type       = $sd['icon_type'];
		$icon_font       = esc_attr( $sd['icon_font'] );
		$icon_img        = $sd['icon_img'];
		$icon_margin     = $sd['icon_margin'];
		$link            = $sd['link'];
		$bg_color        = $sd['bg_color'];
		$title_color     = $sd['title_color'];
		$text_color      = $sd['text_color'];
		$header_color    = $sd['header_color'];
		$icon_bg_color   = $sd['icon_bg_color'];
		$icon_color      = $sd['icon_color'];
		$read_more_color = $sd['read_more_color'];
		
		$info_table_bg       = ( ! empty( $bg_color ) ? 'style="background-color:' . $bg_color . ';"' : '' );
		$table_title_color   = ( ! empty( $title_color ) ? 'style="color:' . $title_color . ';"' : '' );
		$table_header_color  = ( ! empty( $header_color ) ? 'style="background-color:' . $header_color . ';"' : '' );
		$table_icon_bg_color = ( ! empty( $icon_bg_color ) ? 'style="background-color:' . $icon_bg_color . ';"' : '' );
		$table_icon_color    = ( ! empty( $icon_color ) ? 'style="color:' . $icon_color . ';"' : '' );
		$table_more_color    = ( ! empty( $read_more_color ) ? 'style="color:' . $read_more_color . ';"' : '' );
		
		$rand = esc_attr( mt_rand( 10, 1000 ) );
                
//                $body = array();
//                $gmap_request_result = array();
//		if ($gmap_active === true) {
//                    $response = wp_remote_request( 'https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJBaiycSJJnkcRgpcOerc8vnU&key=AIzaSyB68CgwAu-OBi_9WNG5y2yujhPpU-GChic' );
//                    if( is_array($response) ) {
//                      $header = $response['headers']; // array of http header lines
//                      $body = $response['body']; // use the content
//                    }                    
//                }             
		
		ob_start();
	?>
		<?php if ( ! empty( $text_color ) ) : ?>
			<style type = "text/css" scoped>
				.sd-info-table-<?php echo $rand; ?> p {
					color: <?php echo $text_color; ?>;
				}
			</style>
		<?php endif; ?>
		<div class="sd-info-table sd-info-table-<?php echo $rand; ?>" <?php echo $info_table_bg; ?>>
			<span class="sd-info-table-header" <?php echo $table_header_color; ?>></span>
			<div class="sd-info-table-wrapper">
				<div class="sd-info-table-icon">
					<?php if ( $icon_type == 'fi' ) : ?>
						<span class="sd-icon-wrapper" <?php echo $table_icon_bg_color; ?>><i class="<?php echo $icon_font; ?>" <?php echo $table_icon_color; ?>></i></span>
					<?php endif; ?>
					<?php if ( $icon_type == 'ci' ) : ?>
						<?php echo wp_get_attachment_image( $icon_img ); ?>
					<?php endif; ?>
				</div>
				<!-- sd-info-table-icon -->
				<h2 <?php echo $table_title_color; ?>><?php echo $title; ?></h2>
				<?php
                                // Content
                                $shortcodes = '';
                                if ($gmap_active){
                                    
                                    // Request GMap data
                                    $gmap_url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$gmap_place_id.'&key='.$gmap_api_key.'&language=de';
                                    $args = array('timeout' => 120);
                                    $gmap_response = wp_remote_get($gmap_url, $args);
                                    $gmap_response_body = wp_remote_retrieve_body($gmap_response);
                                    $gmap_data = json_decode($gmap_response_body, true);

                                    if(sizeof($gmap_data) > 0)
                                    {                                    
                                        // format adress
                                        $s_street = '';
                                        $a_street_parts = explode(",",$gmap_data["result"]["formatted_address"]);
                                        foreach ($a_street_parts as $part) {
                                            $s_street .= $part.'<br>';
                                        }
                                        // format open hours
                                        $s_open_hours = '';
                                        foreach ($gmap_data["result"]["opening_hours"]["weekday_text"] as $day) {
                                            $s_open_hours .= $day.'<br>';
                                        }
                                    
                                        // Replace all shortcodes with content
                                        $search  = array( '[gmap_name]', 
                                                          '[gmap_adresse]', 
                                                          '[gmap_telefon]', 
                                                          '[gmap_opening_hours]');
                                        $replace = array( $gmap_data["result"]["name"], 
                                                          $s_street, 
                                                          $gmap_data["result"]["international_phone_number"], 
                                                          $s_open_hours);
                                        $content = str_replace($search, $replace, $content);
                                    }
                                }
                                echo $content;
                                
                                ?>
				<?php if ( $read_more == 'yes' ) : ?>
					<?php $href = vc_build_link( $link ); ?>
					<a class="sd-info-table-more" href="<?php echo esc_url( $href['url'] ); ?>" title="<?php echo esc_attr( $href['title'] ); ?>" target="<?php echo esc_attr( $href['target'] ); ?>" <?php echo $table_more_color; ?>><?php echo $read_more_text; ?></a>
				<?php endif; ?>
			</div>		
		</div>
		<!-- sd-info-table -->
		
	
<?php
		return ob_get_clean();	
	}
	add_shortcode( 'sd_info_table_ext','sd_info_table_ext' );
}

// register shortcode to VC

add_action( 'init', 'sd_info_table_ext_vcmap' );

if ( ! function_exists( 'sd_info_table_ext_vcmap' ) ) {
	function sd_info_table_ext_vcmap() {
		vc_map( array(
			'name'              => esc_html__( 'Info Table Ext', 'digitalagency' ),
			'description'       => esc_html__( 'Mit Google Maps verbunden.', 'digitalagency' ),
			'base'              => "sd_info_table_ext",
			'class'             => "sd_info_table_ext",
			'category'          => esc_html__( 'DigitalAgency', 'digitalagency' ),
			'icon'              => "icon-wpb-sd-info-table",
			'admin_enqueue_css' => get_template_directory_uri() . '/framework/inc/vc/assets/css/sd-vc-admin-styles.css',
			'front_enqueue_css' => get_template_directory_uri() . '/framework/inc/vc/assets/css/sd-vc-admin-styles.css',
			'params'            => array(
				array(
					'type'        => 'textfield',
					'class'       => '',
					'heading'     => esc_html__( 'Header Height', 'digitalagency' ),
					'param_name'  => 'header_height',
					'value'       => '100px',
					'description' => esc_html__( 'Insert the header height (default is 100px).', 'digitalagency' ),
				),
				array(
					'type'        => 'textfield',
					'class'       => '',
					'heading'     => esc_html__( 'Title', 'digitalagency' ),
					'param_name'  => 'title',
					'value'       => '',
					'description' => esc_html__( 'Insert the title of the box.', 'digitalagency' ),
				),
				array(
					'type'       => 'dropdown',
					'heading'    => esc_html__( 'GMaps PlaceAPI aktivieren?', 'digitalagency' ),
					'param_name' => 'gmap_active',
					'value'      => array(
						'Yes' => 'yes',
						'No' => 'no',	
					),
					'std'        => 'yes',
					'save_always' => true,
				),                            
				array(
					'type'       => 'textfield',
					'heading'    => esc_html__( 'GMap API Key', 'digitalagency' ),
					'param_name' => 'gmap_api_key',
					'value'      => esc_html__( 'Erforderlich für Verbindung zur GMap API.', 'digitalagency' ),
					'dependency' => array(
						'element' => 'gmap_active',
						'value'   => array( 'yes' ),
					),
                                        'description' => esc_html__( 'Google API Key anfordern => https://developers.google.com/maps/documentation/javascript/get-api-key?hl=de' ),
				),
				array(
					'type'       => 'textfield',
					'heading'    => esc_html__( 'GMap PlaceId', 'digitalagency' ),
					'param_name' => 'gmap_place_id',
					'value'      => esc_html__( 'Erforderlich für GMap DataFeed.', 'digitalagency' ),
					'dependency' => array(
						'element' => 'gmap_active',
						'value'   => array( 'yes' ),
					),
                                        'description' => esc_html__( 'PlaceId rausfinden => https://developers.google.com/places/web-service/place-id' ),
				),                             
				array(
					'type'        => 'textarea_html',
					'class'       => '',
					'heading'     => esc_html__( 'Content', 'digitalagency' ),
					'param_name'  => 'content',
					'value'       => '',
					'description' => esc_html__( 'GMaps shortcodes: [gmap_name] [gmap_adresse] [gmap_telefon] [gmap_opening_hours]',  'digitalagency' ),
				),
				array(
					'type'       => 'dropdown',
					'heading'    => esc_html__( 'Show Read More link?', 'digitalagency' ),
					'param_name' => 'read_more',
					'value'      => array(
						'Yes' => 'yes',
						'No' => 'no',	
					),
					'std'        => 'yes',
					'save_always' => true,
				),
				array(
					'type'       => 'textfield',
					'heading'    => esc_html__( 'Read More Text', 'digitalagency' ),
					'param_name' => 'read_more_text',
					'value'      => esc_html__( 'Read More', 'digitalagency' ),
					'dependency' => array(
						'element' => 'read_more',
						'value'   => array( 'yes' ),
					),
				),
				array(
					'type'       => 'dropdown',
					'heading'    => esc_html__( 'Icon Type', 'digitalagency' ),
					'param_name' => 'icon_type',
					'value'      => array(
					'std'        => 'fi',
						'Font Icon' => 'fi',
						'Custom Image' => 'ci',	
					),
					'save_always' => true,
				),
				array(
					'type' => 'iconpicker',
					'heading' => esc_html__( 'Icon', 'js_composer' ),
					'param_name' => 'icon_font',
					'value' => 'fa fa-adjust', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => true,
						// default true, display an "EMPTY" icon?
						'iconsPerPage' => 200,
						// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'description' => esc_html__( 'Select icon from library.', 'digitalagency' ),
					'dependency' => array(
						'element' => 'icon_type',
						'value'   => array( 'fi' ),
					),
				),
				array(
					'type'        => 'attach_image',
					'class'       => '',
					'heading'     => esc_html__( 'Custom Image Icon', 'digitalagency' ),
					'param_name'  => 'icon_img',
					'description' => esc_html__( 'Upload your custom image icon. (105x105 optimal size)', 'digitalagency' ),
					'dependency' => array(
						'element' => 'icon_type',
						'value'   => array( 'ci' ),
					),
				),
				array(
					'type'        => 'textfield',
					'class'       => '',
					'heading'     => esc_html__( 'Icon Image Top Margin', 'digitalagency' ),
					'param_name'  => 'icon_margin',
					'value'       => '-50px',
					'description' => esc_html__( 'Insert the icon image top margin (default -50px).', 'digitalagency' ),
					'dependency' => array(
						'element' => 'icon_type',
						'value'   => array( 'ci' ),
					),
				),
				array(
					'type'       => 'vc_link',
					'heading'    => esc_html__( 'Link', 'digitalagency' ),
					'param_name' => 'link',
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Background Color', 'digitalagency' ),
					'param_name' => 'bg_color',
					'group'      => esc_html__( 'Styling', 'digitalagency' ),
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Header Color', 'digitalagency' ),
					'param_name' => 'header_color',
					'group'      => esc_html__( 'Styling', 'digitalagency' ),
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Title Color', 'digitalagency' ),
					'param_name' => 'title_color',
					'group'      => esc_html__( 'Styling', 'digitalagency' ),
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Text Color', 'digitalagency' ),
					'param_name' => 'text_color',
					'group'      => esc_html__( 'Styling', 'digitalagency' ),
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Icon Background Color', 'digitalagency' ),
					'param_name' => 'icon_bg_color',
					'group'      => esc_html__( 'Styling', 'digitalagency' ),
					'dependency' => array(
						'element' => 'icon_type',
						'value'   => array( 'fi' ),
					),
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Icon Color', 'digitalagency' ),
					'param_name' => 'icon_color',
					'group'      => esc_html__( 'Styling', 'digitalagency' ),
					'dependency' => array(
						'element' => 'icon_type',
						'value'   => array( 'fi' ),
					),
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Read More Color', 'digitalagency' ),
					'param_name' => 'read_more_color',
					'group'      => esc_html__( 'Styling', 'digitalagency' ),
					'dependency' => array(
						'element' => 'read_more',
						'value'   => array( 'yes' ),
					),
				),
			),
		));
	}
}